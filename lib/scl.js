var FC = FC || {};
FC.SCL = (function () { //the standard component library
    var _library = {
            SaveCancel: function (container) {
                var _private = {
                        save: document.createElement('button'),
                        cancel: document.createElement('button'),
                        handlers: {
                            'saveClicked': [],
                            'cancelClicked': []
                        },
                        dispatchEvent: function(evt) {
                            _private.handlers[evt.type].forEach(function (handler) {
                                handler(evt);
                            });
                        }
                    },
                    _public = {
                        saveButton: _private.save,
                        cancelButton: _private.cancel,
                        addEventListener: function (eventType, listener) {
                            _private.handlers[eventType].push(listener);
                        }
                    };

                _private.save.classList.add('scl-hidden');
                _private.save.classList.add('scl-button', 'scl-save-button');
                _private.save.addEventListener('click', function () { 
                    _private.dispatchEvent(new CustomEvent('saveClicked'));
                });
                _private.save.appendChild(document.createTextNode('Save'));

                _private.cancel.classList.add('scl-hidden');
                _private.cancel.classList.add('scl-button', 'scl-cancel-button');
                _private.cancel.addEventListener('click', function () { 
                    _private.dispatchEvent(new CustomEvent('cancelClicked')); 
                });
                _private.cancel.appendChild(document.createTextNode('Cancel'));

                container.appendChild(_private.save);
                container.appendChild(_private.cancel);
                
                return _public;
            },
            AddSaveCancel: function (container) {
                var _private = {
                        add: document.createElement('button'),
                        sc: new App.View.SaveCancel(container),
                        save: null,
                        cancel: null,
                        handlers: {
                            'addClicked': []
                        }
                    },
                    _public = {
                        addButton: _private.add,
                        saveButton: _private.sc.saveButton,
                        cancelButton: _private.sc.cancelButton,
                        addEventListener: function (eventType, listener) {
                            if (eventType !== 'addClicked') {
                                _private.sc.addEventListener(eventType, listener);
                            } else {
                                _private.handlers[eventType].push(listener);
                            }
                        }
                    };

                return _public;
            },
            EditSaveCancel: function (container) {
                var _private = {
                        div: document.createElement('div'),
                        edit: document.createElement('button'),
                        sc: null,
                        handlers: {
                            'editClicked': [],
                            'saveClicked': [],
                            'cancelClicked': []
                        },
                        dispatchEvent: function(evt) {
                            _private.handlers[evt.type].forEach(function (listener) {
                                listener(evt);
                            });
                        }
                    },
                    _public = {
                        toggleDisplay: function () {
                            var buttons = [this.editButton, this.saveButton, this.cancelButton];
                            buttons.map(function (button) {
                                button.classList.toggle('scl-hidden');
                                button.classList.toggle('scl-visible');
                            });
                        },
                        editButton: _private.edit,
                        saveButton: null,
                        cancelButton: null,
                        addEventListener: function (eventType, listener) {
                            if (eventType !== 'editClicked') {
                                _private.sc.addEventListener(eventType, listener);
                            } else {
                                _private.handlers[eventType].push(listener);
                            }
                        },
                        setClass: function (button, className) {
                            button.classList.add(className);
                        }
                    };

                    _private.div.classList.add('scl-edit-save-cancel');
                    _private.sc = new FC.SCL.SaveCancel(_private.div);
                    _public.saveButton = _private.sc.saveButton;
                    _public.cancelButton = _private.sc.cancelButton;

                    _private.edit.classList.add('scl-visible');
                    _private.edit.classList.add('scl-button','scl-edit-button');
                    _private.edit.appendChild(document.createTextNode('Edit'));
                    _private.edit.addEventListener('click', function () {
                        _public.toggleDisplay();
                        _private.dispatchEvent(new CustomEvent('editClicked'));
                    });

                    _public.saveButton.addEventListener('click', function () {
                        _public.toggleDisplay();
                    });

                    _public.cancelButton.addEventListener('click', function () {
                        _public.toggleDisplay();
                    });

                    _private.div.appendChild(_private.edit);


                    container.appendChild(_private.div);

                    return _public;
            },
        };
    return _library;
})();
