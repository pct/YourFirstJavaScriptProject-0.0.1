var App = {};

App.Model = (function () {
    var _library = {
        Profile: function (name, imgSrc, hobbies) {
            var _private = {
                    name: name,
                    imgURL: imgSrc,
                    hobbyList: hobbies,
                    handlers: {
                        profileNameChanged: []
                    },
                    dispatchEvent: function(evt) {
                        _private.handlers[evt.type].forEach(function (handler) {
                            handler(evt);
                        });
                    }
                },
                _public = {
                    getName: function () {
                        return _private.name;
                    },
                    setName: function (value) {
                        if (value !== _private.name) {
                            var profileNameChanged = new CustomEvent('profileNameChanged', {detail:{oldValue:_private.name, newValue:value}});
                            _private.name = value;
                            _private.dispatchEvent(profileNameChanged);
                        }
                    },
                    addEventListener: function (eventType, listener) {
                        _private.handlers[eventType].push(listener);
                    },
                    imgURL: _private.imgURL,
                    hobbyList: _private.hobbyList
                };
            return _public;
        }
    };

    return _library;
})();

App.Controller = (function () {
    var _library = {
            Profile: function (model, view) {
                view.addEventListener('profileNameEdited', function (evt) {
                    model.setName(evt.detail.newValue);
                });
            }
        };
    return _library;
})();

App.View = (function (scl) { //the View 'class' library component
    var _library = {
            Profile: function (model, container) {
                         var _private = {
                                 div: document.createElement('div'),
                                 heading: document.createElement('h3'),
                                 input: document.createElement('input'),
                                 img: document.createElement('img'),
                                 hobbies: undefined,
                                 editButtonClickHandler: function () {
                                    _private.heading.classList.remove('visible');
                                    _private.heading.classList.add('hidden');
                                    _private.input.classList.remove('hidden');
                                    _private.input.classList.add('visible');
                                    _private.input.focus();
                                    _private.input.select();
                                 },
                                 saveButtonClickHandler: function () {
                                    _private.heading.classList.add('visible');
                                    _private.heading.classList.remove('hidden');
                                    _private.input.classList.add('hidden');
                                    _private.input.classList.remove('visible');
                                    _private.dispatchEvent(new CustomEvent('profileNameEdited', {detail:{newValue:_private.input.value}}));
                                 },
                                 cancelButtonClickHandler: function () {
                                    _private.heading.classList.add('visible');
                                    _private.heading.classList.remove('hidden');
                                    _private.input.classList.add('hidden');
                                    _private.input.classList.remove('visible');
                                    _private.input.value = model.getName();
                                 },
                                 profileNameChangedHandler: function (evt) {
                                    _private.input.value = model.getName();
                                    _private.heading.textContent = model.getName();
                                 },
                                 handlers: {
                                    'profileNameEdited': []
                                 },
                                 dispatchEvent(evt) {
                                    _private.handlers[evt.type].forEach(function (handler) {
                                        handler(evt);
                                    });

                                 }
                             },
                             _public = {
                                 element: _private.div,
                                 addEventListener: function (eventType, listener) {
                                    if (!Array.isArray(_private.handlers[eventType])) {
                                        _private.handlers[eventType] = [];
                                    }
                                    _private.handlers[eventType].push(listener);
                                 }
                             };
                         _private.div.setAttribute('class', 'profile-view');
                         
                         _private.heading.textContent = model.getName();
                         _private.div.appendChild(_private.heading);
                         
                         _private.input.setAttribute('type', 'text');
                         _private.input.setAttribute('class', 'hidden');
                         _private.input.value = model.getName();
                         _private.div.appendChild(_private.input);

                         _private.input.editComponent = new scl.EditSaveCancel(_private.div);
                         _private.input.editComponent.addEventListener('editClicked', _private.editButtonClickHandler);

                         _private.input.editComponent.addEventListener('saveClicked', _private.saveButtonClickHandler);
                         _private.input.editComponent.addEventListener('cancelClicked', _private.cancelButtonClickHandler);

                         model.addEventListener('profileNameChanged', _private.profileNameChangedHandler);
                         
                         _private.img.setAttribute('src', model.imgURL);
                         _private.div.appendChild(_private.img);

                         //_private.hobbies.render(_private.div);
                         _private.hobbies = new App.View.Hobbies(model.hobbyList, _private.div),
                         _private.div.appendChild(_private.hobbies.element);

                         container.appendChild(_private.div);
                         
                         return _public;
                     },
            Hobbies: function (model, container) {
                         var _private = {
                                 div: document.createElement('div'),
                                 heading: document.createElement('h5'),
                                 list: document.createElement('ul')
                             },
                             _public = {
                                 element: _private.div,
                             };

                        model.map(function (hobby) {
                            var li = document.createElement('li');
                            li.appendChild(document.createTextNode(hobby));
                            _private.list.appendChild(li);
                        });
                        container.appendChild(_private.div);
                        
                         _private.div.setAttribute('class', "hobbies-view");
                         _private.heading.appendChild(document.createTextNode('My hobbies:'));
                         _private.div.appendChild(_private.heading);
                         _private.div.appendChild(_private.list);
                         return _public;
                     }
        };
    return _library;
})(FC.SCL);
